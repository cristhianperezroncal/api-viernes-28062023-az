package com.cperez.testacr.repository.impl;

import com.cperez.testacr.model.Airline;
import com.cperez.testacr.model.Flight;
import com.cperez.testacr.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;



@Repository
public class FlightRepositoryImpl implements FlightRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Flight mapToFlight(ResultSet rs) throws SQLException {
        Flight flight = new Flight();
        flight.setId(rs.getInt("id"));
        Airline airline = new Airline();
        airline.setId(rs.getInt("idAirline"));
        flight.setAirline(airline);
        flight.setFlightNumber(rs.getInt("flightNumber"));
        flight.setOrigin(rs.getString("origin"));
        flight.setDestiny(rs.getString("destiny"));
        return flight;
    }

    @Override
    public List<Flight> getFlights() {
        String sql = "SELECT * FROM Flight";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToFlight(rs));
    }

    @Override
    public Flight getFlightById(int id) {
        String sql = "SELECT * FROM Flight WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToFlight(rs), id);
    }

    @Override
    public Flight getFlightByFlightNumber(int flightNumber) {
        String sql = "SELECT * FROM Flight WHERE flightNumber = ?";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToFlight(rs), flightNumber);
    }

    @Override
    public Flight createdFlight(Flight flight) {
        String sql = "INSERT INTO Flight (idAirline, flightNumber, origin, destiny) VALUES (?, ?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setInt(1, flight.getAirline().getId());
                    ps.setInt(2, flight.getFlightNumber());
                    ps.setString(3, flight.getOrigin());
                    ps.setString(4, flight.getDestiny());
                    return ps;
                }, keyHolder
        );
        flight.setId(keyHolder.getKey().intValue());
        return flight;
    }
}
