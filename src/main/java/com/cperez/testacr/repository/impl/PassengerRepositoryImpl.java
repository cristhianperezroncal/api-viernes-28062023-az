package com.cperez.testacr.repository.impl;

import com.cperez.testacr.model.Flight;
import com.cperez.testacr.model.Passenger;
import com.cperez.testacr.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PassengerRepositoryImpl implements PassengerRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Passenger mapToPassenger(ResultSet rs) throws SQLException {
        Passenger passenger = new Passenger();
        passenger.setId(rs.getInt("id"));


        //flight.setId(rs.getInt("idFlight"));

        passenger.setName(rs.getString("name"));
        passenger.setPhone(rs.getString("phone"));
        passenger.setEmail(rs.getString("email"));
        return passenger;
    }
    @Override
    public List<Passenger> getPassengers() {
        String sql = "SELECT * FROM Passenger";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToPassenger(rs));
    }

    @Override
    public List<Passenger> getPassengersByFlightId(int id) {
        String sql = "SELECT * FROM Passenger WHERE idFlight = ?";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToPassenger(rs), id);
    }

    @Override
    public Passenger getPassengerById(int id) {
        String sql = "SELECT * FROM Passenger WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToPassenger(rs), id);
    }

    @Override
    public Passenger assignPassenger(String name, int idFlight) {
        String updateSql = "UPDATE Passenger SET idFlight = ? WHERE name = ?";
        jdbcTemplate.update(updateSql, idFlight, name);

        String selectSql = "SELECT * FROM Passenger WHERE name = ?";
        return jdbcTemplate.queryForObject(selectSql, (rs, rowNum) -> mapToPassenger(rs), name);
    }

    @Override
    public Passenger getPassengerByName(String name) {
        String sql = "SELECT * FROM Passenger WHERE name = ?";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToPassenger(rs), name);
    }

    @Override
    public Passenger createdPassenger(Passenger passenger) {
        String sql = "INSERT INTO Passenger(name, phone, email) values(?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, passenger.getName());
                    ps.setString(2, passenger.getPhone());
                    ps.setString(3, passenger.getEmail());
                    return ps;
                }, keyHolder
        );
        passenger.setId(keyHolder.getKey().intValue());
        return passenger;
    }
}
