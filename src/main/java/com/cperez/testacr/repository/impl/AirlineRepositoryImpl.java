package com.cperez.testacr.repository.impl;

import com.cperez.testacr.model.Airline;
import com.cperez.testacr.repository.AirlineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class AirlineRepositoryImpl implements AirlineRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Airline> getAirlines() {
        String sql = "SELECT * FROM Airline";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Airline.class));
    }

    @Override
    public Airline getAirlineById(int id) {
        String sql = "SELECT * FROM Airline WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Airline.class));
    }

    @Override
    public Airline createdAirline(Airline airline) {
        String sql = "INSERT INTO Airline (name) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, airline.getName());
                    return ps;
                }, keyHolder
        );
        airline.setId(keyHolder.getKey().intValue());
        return airline;
    }
}
