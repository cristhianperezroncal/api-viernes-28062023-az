package com.cperez.testacr.repository;

import com.cperez.testacr.model.Flight;

import java.util.List;

public interface FlightRepository {
    List<Flight> getFlights();
    Flight getFlightById(int id);

    Flight getFlightByFlightNumber(int flightNumber);
    Flight createdFlight(Flight flight);
}
