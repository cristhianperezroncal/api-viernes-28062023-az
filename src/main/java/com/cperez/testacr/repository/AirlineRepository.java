package com.cperez.testacr.repository;

import com.cperez.testacr.dto.AirlineDTO;
import com.cperez.testacr.model.Airline;

import java.util.List;
import java.util.Optional;


public interface AirlineRepository {
    List<Airline> getAirlines();
    Airline getAirlineById(int id);
    Airline createdAirline(Airline airline);

}
