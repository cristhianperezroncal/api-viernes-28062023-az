package com.cperez.testacr.repository;

import com.cperez.testacr.model.Flight;
import com.cperez.testacr.model.Passenger;

import java.util.List;

public interface PassengerRepository {
    List<Passenger> getPassengers();
    List<Passenger> getPassengersByFlightId(int id);
    Passenger getPassengerById(int id);

    Passenger assignPassenger(String name, int idFlight);

    Passenger getPassengerByName(String name);
    Passenger createdPassenger(Passenger passenger);
}
