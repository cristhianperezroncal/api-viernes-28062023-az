package com.cperez.testacr;

import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;
import com.azure.security.keyvault.secrets.models.KeyVaultSecret;
import com.cperez.testacr.credential.KeyVaultConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TestacrApplication {
	@Autowired
	private KeyVaultConfig keyVaultConfig;

	public static void main(String[] args) {

		SpringApplication.run(TestacrApplication.class, args);

	}

	@Component
	public class MyComponent {
		private final SecretClient secretClient;

		public MyComponent(SecretClient secretClient) {
			this.secretClient = secretClient;
		}

		@PostConstruct
		public void init() {
			KeyVaultSecret secret = secretClient.getSecret("secretUrl");
			String secretValue = secret.getValue();
			System.out.println("Secreto: "+secretValue);
			// hacer algo con el valor del secreto...
		}
	}

}
