package com.cperez.testacr.credential;
import com.azure.core.credential.BasicAuthenticationCredential;
import com.azure.identity.DefaultAzureCredential;
import com.azure.identity.DefaultAzureCredentialBuilder;
import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeyVaultConfig {
    @Value("${azure.key-vault.uri}")
    private String keyVaultUri;
    @Bean
    public SecretClient secretClient() {
        DefaultAzureCredential credential = new DefaultAzureCredentialBuilder().build();

        return new SecretClientBuilder()
                .vaultUrl(keyVaultUri)
                .credential(credential)
                .buildClient();
    }
}
