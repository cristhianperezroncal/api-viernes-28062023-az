package com.cperez.testacr.model;

import com.cperez.testacr.dto.FlightDTO;
import com.cperez.testacr.dto.PassengerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flight {
    private int id;
    private Airline airline;
    private int flightNumber;
    private String origin;
    private String destiny;
    //private List<PassengerDTO> passengers;
    private List<Passenger> passengers;

    public Flight(FlightDTO flightDTO) {
        Airline airline = new Airline();
        airline.setId(flightDTO.getIdAirline());
        this.airline = airline;
        this.flightNumber = flightDTO.getFlightNumber();
        this.origin = flightDTO.getOrigin();
        this.destiny = flightDTO.getDestiny();
    }

}
