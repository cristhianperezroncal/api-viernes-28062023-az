package com.cperez.testacr.model;

import com.cperez.testacr.dto.AirlineDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Airline {
    private int id;
    private String name;
    public Airline(AirlineDTO airlineDTO) {
        this.name = airlineDTO.getName();
    }
}
