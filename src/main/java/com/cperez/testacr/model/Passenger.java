package com.cperez.testacr.model;

import com.cperez.testacr.dto.PassengerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Passenger {
    private int id;
    private String name;
    private String phone;
    private String email;
    public Passenger(PassengerDTO passengerDTO) {
        this.name = passengerDTO.getName();;
        this.phone = passengerDTO.getPhone();
        this.email = passengerDTO.getEmail();
    }
}
