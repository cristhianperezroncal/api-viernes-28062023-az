package com.cperez.testacr.service;

import com.cperez.testacr.dto.AssignPassengerDTO;
import com.cperez.testacr.dto.PassengerDTO;
import com.cperez.testacr.model.Passenger;

import java.util.List;

public interface PassengerService {
    List<Passenger> getPassengers();
    Passenger getPassengerById(int id);
    Passenger getPassengerByName(String name);
    Passenger assignPassenger(AssignPassengerDTO assignPassengerDTO);
    Passenger createdPassenger(PassengerDTO passengerDTO);
}
