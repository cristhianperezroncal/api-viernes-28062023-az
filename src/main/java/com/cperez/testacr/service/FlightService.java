package com.cperez.testacr.service;

import com.cperez.testacr.dto.FlightDTO;
import com.cperez.testacr.model.Flight;

import java.util.List;
import java.util.Optional;

public interface FlightService {
    List<Flight> getFlights();
    Flight getFlightById(int id);
    Flight getFlightByFlightNumber(int flightNumber);
    Flight createdFlight(FlightDTO flightDTO);
}
