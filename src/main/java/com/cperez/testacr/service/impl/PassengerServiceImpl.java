package com.cperez.testacr.service.impl;

import com.cperez.testacr.dto.AssignPassengerDTO;
import com.cperez.testacr.dto.PassengerDTO;
import com.cperez.testacr.model.Flight;
import com.cperez.testacr.model.Passenger;
import com.cperez.testacr.repository.FlightRepository;
import com.cperez.testacr.repository.PassengerRepository;
import com.cperez.testacr.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private FlightRepository flightRepository;
    @Override
    public List<Passenger> getPassengers() {
        return passengerRepository.getPassengers();
    }

    @Override
    public Passenger getPassengerById(int id) {
        return passengerRepository.getPassengerById(id);
    }

    @Override
    public Passenger getPassengerByName(String name) {
        return passengerRepository.getPassengerByName(name);
    }

    @Override
    public Passenger assignPassenger(AssignPassengerDTO assignPassengerDTO) {
        String name = assignPassengerDTO.getPassengerName();

        Flight flight = flightRepository.getFlightByFlightNumber(assignPassengerDTO.getFlightNumber());

        int flightNumber =  flight.getId();

        return passengerRepository.assignPassenger(name, flightNumber);
    }

    @Override
    public Passenger createdPassenger(PassengerDTO passengerDTO) {
        Passenger passenger = new Passenger(passengerDTO);
        return passengerRepository.createdPassenger(passenger);
    }
}
