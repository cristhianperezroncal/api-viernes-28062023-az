package com.cperez.testacr.service.impl;

import com.cperez.testacr.dto.AirlineDTO;
import com.cperez.testacr.model.Airline;
import com.cperez.testacr.repository.AirlineRepository;
import com.cperez.testacr.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirlineServiceImpl implements AirlineService {
    @Autowired
    private AirlineRepository airlineRepository;
    @Override
    public List<Airline> getAirlines() {
        return airlineRepository.getAirlines();
    }

    @Override
    public Airline getAirlineById(int id) {
        return airlineRepository.getAirlineById(id);
    }

    @Override
    public Airline createdAirline(AirlineDTO airlineDTO) {
        Airline airline = new Airline(airlineDTO);
        return airlineRepository.createdAirline(airline);
    }
}
