package com.cperez.testacr.service.impl;

import com.cperez.testacr.dto.FlightDTO;
import com.cperez.testacr.model.Airline;
import com.cperez.testacr.model.Flight;
import com.cperez.testacr.repository.AirlineRepository;
import com.cperez.testacr.repository.FlightRepository;
import com.cperez.testacr.repository.PassengerRepository;
import com.cperez.testacr.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlightServiceImpl implements FlightService {
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private AirlineRepository airlineRepository;

    @Override
    public List<Flight> getFlights() {
        List<Flight> flights = flightRepository.getFlights();
        for (Flight flight : flights) {
            flight.setAirline(airlineRepository.getAirlineById(flight.getAirline().getId()));
            flight.setPassengers(passengerRepository.getPassengersByFlightId(flight.getId()));
        }
        return flights;
    }

    @Override
    public Flight getFlightById(int id) {
        Flight flight = flightRepository.getFlightById(id);
        flight.setAirline(airlineRepository.getAirlineById(flight.getAirline().getId()));
        flight.setPassengers(passengerRepository.getPassengersByFlightId(id));
        return flight;
    }

    @Override
    public Flight getFlightByFlightNumber(int flightNumber) {
        Flight flight = flightRepository.getFlightByFlightNumber(flightNumber);
        flight.setAirline(airlineRepository.getAirlineById(flight.getAirline().getId()));
        flight.setPassengers(passengerRepository.getPassengersByFlightId(flight.getId()));
        return flight;
    }

    @Override
    public Flight createdFlight(FlightDTO flightDTO) {
        Flight flight = new Flight(flightDTO);
        flight.setAirline(airlineRepository.getAirlineById(flightDTO.getIdAirline()));
        return flightRepository.createdFlight(flight);
    }
}
