package com.cperez.testacr.service;

import com.cperez.testacr.dto.AirlineDTO;
import com.cperez.testacr.model.Airline;

import java.util.List;

public interface AirlineService {
    List<Airline> getAirlines();
    Airline getAirlineById(int id);
    Airline createdAirline(AirlineDTO airlineDTO);
}
