package com.cperez.testacr.controller;

import com.cperez.testacr.dto.AirlineDTO;
import com.cperez.testacr.model.Airline;
import com.cperez.testacr.service.AirlineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/v1/airlines")
@RequiredArgsConstructor
public class AirlineController {
    @Autowired
    private AirlineService airlineService;

    @GetMapping
    public ResponseEntity<List<Airline>> getAirlines() {
        try {
            List<Airline> airlines = airlineService.getAirlines();
            return ResponseEntity.status(HttpStatus.OK).body(airlines);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAirlineById(@PathVariable int id) {
        try {
            Airline airline = airlineService.getAirlineById(id);
            return ResponseEntity.status(HttpStatus.OK).body(airline);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping
    public ResponseEntity<Airline> createdAirline(@RequestBody AirlineDTO airlineDTO) {
        try {
            Airline airline = airlineService.createdAirline(airlineDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(airline);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }



}
