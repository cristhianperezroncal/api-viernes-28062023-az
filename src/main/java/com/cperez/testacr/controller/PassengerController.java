package com.cperez.testacr.controller;

import com.cperez.testacr.dto.AssignPassengerDTO;
import com.cperez.testacr.dto.FlightDTO;
import com.cperez.testacr.dto.PassengerDTO;
import com.cperez.testacr.model.Flight;
import com.cperez.testacr.model.Passenger;
import com.cperez.testacr.service.FlightService;
import com.cperez.testacr.service.PassengerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/passengers")
@RequiredArgsConstructor
public class PassengerController {
    @Autowired
    private PassengerService passengerService;

    @GetMapping
    public ResponseEntity<List<Passenger>> getPassengers() {
        try {
            List<Passenger> passengers = passengerService.getPassengers();
            return ResponseEntity.status(HttpStatus.OK).body(passengers);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPassengerById(@PathVariable int id) {
        try {
            Passenger passenger = passengerService.getPassengerById(id);
            return ResponseEntity.status(HttpStatus.OK).body(passenger);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<?> getPassengerByName(@PathVariable String name) {
        try {
            Passenger passenger = passengerService.getPassengerByName(name);
            return ResponseEntity.status(HttpStatus.OK).body(passenger);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping
    public ResponseEntity<Passenger> createdPassenger(@RequestBody PassengerDTO passengerDTO) {
        try {
            Passenger passenger = passengerService.createdPassenger(passengerDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(passenger);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping("/assignPassenger")
    public ResponseEntity<Passenger> assignPassenger(@RequestBody AssignPassengerDTO assignPassengerDTO) {
        try {
            Passenger passenger = passengerService.assignPassenger(assignPassengerDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(passenger);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }



}
