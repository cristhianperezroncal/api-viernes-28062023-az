package com.cperez.testacr.controller;

import com.cperez.testacr.dto.AirlineDTO;
import com.cperez.testacr.dto.FlightDTO;
import com.cperez.testacr.model.Airline;
import com.cperez.testacr.model.Flight;
import com.cperez.testacr.service.FlightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/v1/fligths")
@RequiredArgsConstructor
public class FlightController {
    @Autowired
    private FlightService flightService;

    @GetMapping
    public ResponseEntity<List<Flight>> getFlights() {
        try {
            List<Flight> flights = flightService.getFlights();
            return ResponseEntity.status(HttpStatus.OK).body(flights);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getFlightById(@PathVariable int id) {
        try {
            Flight flight = flightService.getFlightById(id);
            return ResponseEntity.status(HttpStatus.OK).body(flight);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/flightNumber/{flightNumber}")
    public ResponseEntity<?> getFlightByFlightNumber(@PathVariable int flightNumber) {
        try {
            Flight flight = flightService.getFlightByFlightNumber(flightNumber);
            return ResponseEntity.status(HttpStatus.OK).body(flight);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping
    public ResponseEntity<Flight> createdFlight(@RequestBody FlightDTO flightDTO) {
        try {
            Flight flight = flightService.createdFlight(flightDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(flight);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
}
