package com.cperez.testacr.dto;

import lombok.Data;

@Data
public class AssignPassengerDTO {
    private String passengerName;
    private int flightNumber;
}
