package com.cperez.testacr.dto;

import com.cperez.testacr.model.Airline;
import lombok.Data;

@Data
public class FlightDTO {
    private int idAirline;
    private int flightNumber;
    private String origin;
    private String destiny;
}
