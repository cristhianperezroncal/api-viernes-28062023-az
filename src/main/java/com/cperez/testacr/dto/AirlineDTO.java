package com.cperez.testacr.dto;

import lombok.Data;

@Data
public class AirlineDTO {
    private String name;
}
