use Airlines;

-- Crear la tabla Airline
CREATE TABLE Airline (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);

-- Crear la tabla Flight
CREATE TABLE Flight (
  id INT(11) NOT NULL AUTO_INCREMENT,
  idAirline INT(11),
  flightNumber INT(11),
  origin VARCHAR(255),
  destiny VARCHAR(255),
  PRIMARY KEY (id),
  FOREIGN KEY (idAirline) REFERENCES Airline(id)
);

-- Crear la tabla Passenger
CREATE TABLE Passenger (
  id INT(11) NOT NULL AUTO_INCREMENT,
  idFlight INT(11),
  name VARCHAR(255),
  phone VARCHAR(255),
  email VARCHAR(255),
  PRIMARY KEY (id),
  FOREIGN KEY (idFlight) REFERENCES Flight(id)
);

-- Insertar datos en la tabla Airline
INSERT INTO Airline (name) VALUES
('American Airlines'),
('Delta Air Lines'),
('United Airlines');

-- Insertar datos en la tabla Flight
INSERT INTO Flight (idAirline, flightNumber, origin, destiny) VALUES
(1, 1234, 'New York', 'Los Angeles'),
(2, 5678, 'Atlanta', 'Seattle'),
(3, 9012, 'Chicago', 'San Francisco');

-- Insertar datos en la tabla Passenger
INSERT INTO Passenger (idFlight, name, phone, email) VALUES
(1, 'John Smith', '123-456-7890', 'john.smith@example.com'),
(2, 'Mary Johnson', '987-654-3210', 'mary.johnson@example.com'),
(3, 'Robert Davis', '555-555-5555', 'robert.davis@example.com');

SELECT * FROM Airline;
SELECT * FROM Flight;
SELECT * FROM Passenger;